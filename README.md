# Objetivo
La finalidad de este proyecto es poner a disposición los modulos que son componentes empaquetados y utlizados por otras aplicaciones. 

* finanzasswitcher
* eir tambien conocido como formCreator
* ittenModels
* omws


# Como este proyecto logra empaquetar estos componentes ?

En el yml de .gitlab-ci.yml de los proyectos anteriormente mencionados hay un stage que se enarga de empaqueralo y subirlo aqui 

* http://gitlab.sanluispotosi.gob.mx/DevTeam/finanzasswitcher/-/blob/master/.gitlab-ci.yml#L58
* http://gitlab.sanluispotosi.gob.mx/DevTeam/eir/-/blob/master/.gitlab-ci.yml#L7
* http://gitlab.sanluispotosi.gob.mx/DevTeam/ittenModels/-/blob/master/.gitlab-ci.yml#L7
* http://gitlab.sanluispotosi.gob.mx/DevTeam/omws/-/blob/master/.gitlab-ci.yml#L7



# Por que se hace asi  ? 

Al monento de que en un proyecto se instalan las depednecias con pip listadas en el archivo requirements.txt  es depedencia debe existir en  https://pypi.org/ o  estar en un repositorio al cual se pueda acceder o pip pueda resolver, 
por este motivo y para no subir estos componetnes a https://pypi.org/



# Como se usan estos empaquetados  ?

Se pone la url al empaquetado que se necesite en el archivo requirements.txt  del proyecto que usara el componente empaquetado en cuestion 

Ejemplo: este archivo de requirements.txt 


```
Django==2.2.19
django-appconf==1.0.4
django-celery-email==3.0.0
IttenModels @ http://gitlab.sanluispotosi.gob.mx/usi/dist-packages/raw/master/ittenModels/qa/IttenModels-1.1.0.tar.gz
```


